#!/usr/bin/env ruby

class GenerateQAJobs
  def execute
    <<~YML
      image: ruby:latest

      stages:
        - test

      job-without-rules:
        stage: test
        script:
          - echo $FOO

      job-with-rules:
        stage: test
        extends:
          - .rules:foo
        script:
          - echo $FOO

      #########
      # Rules #
      #########
      .if-foo-var-specified: &if-foo-var-specified
        if: '$FOO != null && $FOO != ""'

      .if-foo-var-not-specified: &if-foo-var-not-specified
        if: '$FOO == null || $FOO == ""'

      .rules:foo:
        rules:
          - <<: *if-foo-var-not-specified
            when: manual
          - <<: *if-foo-var-specified
            when: always

    YML
  end
end

jobs = GenerateQAJobs.new.execute

File.open('generated-qa-jobs.yml', 'w') { |f| f.write(jobs) }
